package com.daohuyen.dell.store_cosmetics.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daohuyen.dell.store_cosmetics.R;
import com.daohuyen.dell.store_cosmetics.common.Constants;
import com.daohuyen.dell.store_cosmetics.model.Profile;
import com.daohuyen.dell.store_cosmetics.model.User;
import com.daohuyen.dell.store_cosmetics.view.login.LoginActivity;

public class RegisterActivity extends AppCompatActivity {
    EditText etEmail;
    EditText etPass;
    EditText etConfirmPassword;
    EditText etAddress;
    EditText etName;
    Button btRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etEmail= findViewById(R.id.et_email);
        etPass= findViewById(R.id.et_pass);
        etConfirmPassword= findViewById(R.id.et_comfirm);
        etAddress= findViewById(R.id.et_address);
        etName= findViewById(R.id.et_name);
        btRegister= findViewById(R.id.bt_register);
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
                view.startAnimation(animation);
                registerUser();
            }
        });
    }
    private void registerUser(){
        if (etEmail.getText().toString().trim().equals("")) {
            etEmail.setError("Bạn nhập email");
            return;
        }else if(etPass.getText().toString().trim().equals("")){
            etPass.setError("Bạn nhập pass");
            return;
        }else if(!etConfirmPassword.getText().toString().equals(etPass.getText().toString())){
            etConfirmPassword.setError("Bạn pass không trùng khớp");
            return;
        }else if(etAddress.getText().toString().equals("")){
            etAddress.setError("Bạn nhập address");
            return;
        }else if(etName.getText().toString().equals("")) {
            etName.setError("Bạn nhập name");
            return;
        }
        User user= new User(etEmail.getText().toString().trim(), etPass.getText().toString().trim());
        Profile profile= new Profile(user, etName.getText().toString().trim(), etAddress.getText().toString());
        if(LoginActivity.userHelper.registerUser(profile)){
            Intent intent= new Intent(RegisterActivity.this, LoginActivity.class);
            intent.putExtra(Constants.KEY_EMAIL,user.getEmail());
            startActivity(intent);
            finish();
        }else {
            Toast.makeText(RegisterActivity.this, "Lỗi nhập lại",Toast.LENGTH_SHORT).show();
        }
    }
}
