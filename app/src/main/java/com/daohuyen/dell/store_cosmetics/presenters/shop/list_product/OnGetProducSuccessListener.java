package com.daohuyen.dell.store_cosmetics.presenters.shop.list_product;


import com.daohuyen.dell.store_cosmetics.model.PageList;
import com.daohuyen.dell.store_cosmetics.model.view.ProductViewModel;

public interface OnGetProducSuccessListener {
    void onGetPageProductPreviewsSuccess(PageList<ProductViewModel> clothesPreviewPageList);
    void onMessageEror(String msg);
}
