package com.daohuyen.dell.store_cosmetics;

import android.app.Dialog;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daohuyen.dell.store_cosmetics.common.BottomNavigationViewHelper;
import com.daohuyen.dell.store_cosmetics.common.Constants;
import com.daohuyen.dell.store_cosmetics.common.Utils;
import com.daohuyen.dell.store_cosmetics.common.ViewPageAdapter;
import com.daohuyen.dell.store_cosmetics.custom.LoadingDialog;
import com.daohuyen.dell.store_cosmetics.model.Profile;
import com.daohuyen.dell.store_cosmetics.services.eventbus.HeaderProfileEvent;
import com.daohuyen.dell.store_cosmetics.view.ProfileActivity;
import com.daohuyen.dell.store_cosmetics.view.login.LoginActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , BottomNavigationView.OnNavigationItemSelectedListener{
    BottomNavigationView bottomNavigationView;
    NavigationView navigationView;
    ViewPager viewPager;
    private LoadingDialog loadingDialog;
    View userHeaderView;
    private SoundPool soundPool;
    private boolean loaded;
    private AudioManager audioManager;

    private int soundIdDestroy;
    private int soundIdGun;
    private float volume;
    private static final int streamType = AudioManager.STREAM_MUSIC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        EventBus.getDefault().register(this);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        viewPager = findViewById(R.id.view_pager);
        final ViewPageAdapter viewPageAdapter= new ViewPageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPageAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomNavigationView.setSelectedItemId(viewPageAdapter.getMenuIDByPosition(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        loadingDialog = new LoadingDialog(this);
        navigationView.setNavigationItemSelectedListener(this);
        bottomNavigationView= findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        navigationView.removeHeaderView(userHeaderView);
        userHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        Profile headerProfile = Utils.getHeaderProfile(this);
        if (headerProfile.getName() != null) {
            showHeaderProfile(headerProfile);
        }
        switchNavigationDrawer(Constants.LOGIN_TRUE.equals(Utils.getSharePreferenceValues(this,Constants.STATUS_LOGIN)));
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        // Chỉ số âm lượng hiện tại của loại luồng nhạc cụ thể (streamType).
        float currentVolumeIndex = (float) audioManager.getStreamVolume(streamType);


        // Chỉ số âm lượng tối đa của loại luồng nhạc cụ thể (streamType).
        float maxVolumeIndex  = (float) audioManager.getStreamMaxVolume(streamType);

        // Âm lượng  (0 --> 1)
        this.volume = currentVolumeIndex / maxVolumeIndex;

        // Cho phép thay đổi âm lượng các luồng kiểu 'streamType' bằng các nút
        // điều khiển của phần cứng.
        this.setVolumeControlStream(streamType);
        if (Build.VERSION.SDK_INT >= 21 ) {

            AudioAttributes audioAttrib = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            SoundPool.Builder builder= new SoundPool.Builder();
            builder.setAudioAttributes(audioAttrib).setMaxStreams(5);

            this.soundPool = builder.build();
        }
        // Với phiên bản Android SDK < 21
        else {
            // SoundPool(int maxStreams, int streamType, int srcQuality)
            this.soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }

        // Sự kiện SoundPool đã tải lên bộ nhớ thành công.
        this.soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded = true;
            }
        });
        this.soundIdDestroy = this.soundPool.load(this, R.raw.smsthongbao,1);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    private void addBooks(){
        final Dialog dialog= new Dialog(this);
        dialog.setTitle("Thêm Sách");
        dialog.setContentView(R.layout.dialog_add_book);
        dialog.show();
        Button btadd, btclose;
        final EditText etName,etCost;
        final Spinner spType;
        btadd = dialog.findViewById(R.id.bt_add_clothes);
        btclose = dialog.findViewById(R.id.bt_close);
        etName= dialog.findViewById(R.id.et_name);
        spType= dialog.findViewById(R.id.spinner_type);
        etCost= dialog.findViewById(R.id.et_cost);

        btadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                }


        });
        btclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case R.id.action_settings: {
                break;
            }
            case R.id.menu_add: {
                addBooks();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onDestroy() {
        //  Log.i(TAG, "onDestroy: ");
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.nav_login: {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
            case R.id.nav_logout: {
                Utils.setSharePreferenceValues(this, Constants.STATUS_LOGIN, Constants.LOGIN_FAIL);
                Utils.saveHeaderProfile(this, null);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
               // EventBus.getDefault().post(new UserAuthorizationChangedEvent());
                break;
            }
            case R.id.nav_account: {
                startActivity(new Intent(this, ProfileActivity.class));
//                if (Utils.checkNetwork(this)) {
//                    startActivity(new Intent(this, ProfileActivity.class));
//                } else {
//                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
//                }
                break;
            }
            case R.id.nav_favor:{
//                startActivity(new Intent(MainActivity.this, ListBookFavorActivity.class));
                break;
            }
            case R.id.nav_share: {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "Kidd Store");
                    String sAux = "\nỨng dụng bán hàng online\n\n";
                    sAux = sAux + "http://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=vi \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "Chọn cách thức chia sẻ"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.nav_facebook: {
                if (Utils.checkNetwork(this)) {
                    String url = "https://www.facebook.com/NguyenMinhHieu";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } else {
                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.nav_change_password: {
//                if (Utils.checkNetwork(this)) {
//                    startActivity(new Intent(this, ChangePasswordActivity.class));
//                } else {
//                    Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
//                }
//                break;
            }
            case R.id.nav_type_book:{
                viewPager.setCurrentItem(0);
                break;
            }
            case R.id.nav_get_book:{
                viewPager.setCurrentItem(1);
                break;
            }
            case R.id.nav_search_book:{
                viewPager.setCurrentItem(2);
                break;
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void switchNavigationDrawer(boolean isLoggedIn) {
        navigationView.getMenu().clear();
        navigationView.removeHeaderView(userHeaderView);
        if (isLoggedIn) {
            userHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_auth);
            navigationView.inflateMenu(R.menu.menu_navigation_login);
            Utils.setSharePreferenceValues(this, Constants.STATUS_LOGIN, Constants.LOGIN_TRUE);
        } else {
            userHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_no_auth);
            navigationView.inflateMenu(R.menu.menu_navigation_logout);
            Utils.setSharePreferenceValues(this, Constants.STATUS_LOGIN, Constants.LOGIN_FAIL);
        }
    }
    public void showHeaderProfile(Profile headerProfile) {
        ImageView imageView = userHeaderView.findViewById(R.id.img_avatar);
        TextView txtFullname = userHeaderView.findViewById(R.id.txt_full_name);
        TextView txtEmail = userHeaderView.findViewById(R.id.txt_email);
        if (headerProfile != null) {
            Glide.with(this)
                    .load(headerProfile.getSrcAvatar())
                    .apply(new RequestOptions().placeholder(R.drawable.avatar_placeholder).error(R.drawable.avatar_placeholder))
                    .into(imageView);
            txtEmail.setText(headerProfile.getUser().getEmail());
            txtFullname.setText(headerProfile.getName());
        }

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceivedHeaderUpdateEvent(HeaderProfileEvent headerProfileEvent) {
        Utils.saveHeaderProfile(MainActivity.this, headerProfileEvent.getHeaderProfile());
        showHeaderProfile(headerProfileEvent.getHeaderProfile());
    }

}
