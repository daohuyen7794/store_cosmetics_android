package com.daohuyen.dell.store_cosmetics.view.one_product_cart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daohuyen.dell.store_cosmetics.R;
import com.daohuyen.dell.store_cosmetics.model.view.ProductViewModel;

import butterknife.BindView;

public class ProductCartActivity extends AppCompatActivity {
    public static String TAG = "ProductCartActivity";

    @BindView(R.id.img_product_detail)
    ImageView img_productdetail;
    @BindView(R.id.tv_name_product)
    TextView tv_name_product;
    @BindView(R.id.tv_cost_product)
    TextView tv_cost_product;
    @BindView(R.id.edt_number)
    EditText edt_number;
    @BindView(R.id.bt_them)
    Button bt_them;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_cart);
        ProductViewModel productViewModel= (ProductViewModel) getIntent().getSerializableExtra("product");
        Log.d(TAG,productViewModel.getName());
        Log.d(TAG,productViewModel.getLogoUrl());
        Log.d(TAG,productViewModel.getPrice()+"");

//        Toast.makeText(this, ""+productViewModel.getName(), Toast.LENGTH_SHORT).show();
//        Glide.with(this).load(productViewModel.getLogoUrl())
//                .apply(new RequestOptions().placeholder(R.drawable.logoapp))
//                .into(img_productdetail);
        tv_name_product.setText(productViewModel.getName());
        tv_cost_product.setText(productViewModel.getPrice()+" ");

    }
}
