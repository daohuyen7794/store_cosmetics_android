package com.daohuyen.dell.store_cosmetics.presenters.login;


import com.daohuyen.dell.store_cosmetics.model.Customer;

public interface OnGetLoginSuccessListener {
    void onSuccess(Customer customer);
    void onError(String msg);
}
