package com.daohuyen.dell.store_cosmetics.presenters.shop.detail_product;

import android.content.Context;
import android.widget.Toast;

import com.daohuyen.dell.store_cosmetics.model.view.ProductViewModel;
import com.daohuyen.dell.store_cosmetics.view.product_detail.ProductDetailView;


public class ProductDetailPresenterIpl implements ProductDetailPresenter {
    private Context context;
    private ProductDetailView productDetailView;
    private ProductDetailInterator productDetailInterator;

    public ProductDetailPresenterIpl(Context context, ProductDetailView productDetailView) {
        this.context = context;
        this.productDetailView = productDetailView;
        this.productDetailInterator=new ProductDetailInteratorIpl(context);
    }

    @Override
    public void fetchProductDetail(String productID) {
        productDetailView.showProgress();
        productDetailInterator.getProductDetail(productID, new OnGetProductDetailCompleteListener() {
            @Override
            public void onGetProductDetailComplete(ProductViewModel productViewModel) {
                productDetailView.hideProgress();
                productDetailView.showProductDetail(productViewModel);
            }

            @Override
            public void onMessageEror(String msg) {
                productDetailView.hideProgress();
                productDetailView.showErrorLoading(msg);

            }
        });

    }

    @Override
    public void getClothesState(String productID) {
        productDetailView.showProgress();
        productDetailInterator.getProductState(productID, new OnGetProductStateSuccessListener() {
            @Override
            public void onGetStateSuccess(boolean state) {
                productDetailView.showProgress();
                productDetailView.showProductState(state);
            }

            @Override
            public void onError(String msg) {
                productDetailView.hideProgress();
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onViewDestroy() {
        this.productDetailInterator.onViewDestroy();

    }
}
