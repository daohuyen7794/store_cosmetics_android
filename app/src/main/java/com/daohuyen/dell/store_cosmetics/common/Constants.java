package com.daohuyen.dell.store_cosmetics.common;

public class Constants {

    public static final String STATUS_LOGIN = "STATUS_LOGIN";
    public static final String LOGIN_FAIL = "LOGIN_FAIL";
    public static final String KEY_EMAIL = "KEY_EMAIL";
    public static final String TYPE_BOOK = "TYPE_BOOK";
    public static final String KEY_PROFILE = "KEY_PROFILE";
    public static final String USER_NAME = "USER_NAME";
    public static final String CUSTOMER_ID = "CUSTOMER_ID";
    public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    public static final String PRE_AVATAR_URL = "PRE_AVATAR_URL";
    public static final String PRE_FULL_NAME = "PRE_FULL_NAME";
    public static final String PRE_EMAIL = "PRE_EMAIL";
    public static final String PRE_ADDRESS = "PRE_ADDRESS";
    public static final String HEADER_PROFILE = "HEADER_PROFILE";
    public static final String PROFILE = "PROFILE";
    public static final String LOGIN_TRUE = "LOGIN_TRUE";

    public static final String KEY_BOOK = "KEY_BOOK";
    public static final String KEY_SRC = "KEY_SRC";
    public static final int PAGE_SIZE = 10;

    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String KEY_PRODUCT_ID = "KEY_PRODUCT_ID";
}
