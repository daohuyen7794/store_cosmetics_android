package com.daohuyen.dell.store_cosmetics.view.product_detail;


import com.daohuyen.dell.store_cosmetics.model.view.ProductViewModel;

import java.util.List;

public interface ProductDetailView {
    void showProgress();
    void hideProgress();
    void showProductDetail(ProductViewModel productViewModel);
    void showErrorLoading(String message);

    void showProgressSimilarClothes();
    void hideProgressSimilarClothes();

    void showErrorSimilarClothes();
    void hideErrorSimilarClothes();

    void showSimilarLoadingMoreProgress();
    void hideSimilarLoadingMoreProgress();

    void enableLoadingMore(boolean enable);

    void refreshSimilarClothes(List<ProductDetailView> similarClothes);
    void loadmoreSimilarClothes(List<ProductDetailView> similarClothes);

    void switchButtonSaveJobToSaved();
    void switchButtonSaveJobToUnSaved();

    void showListSimilarClothes();
    void hideListSimilarClothes();

    void hideRatingDialog();

    //void getAllRateClothes(List<RateClothesViewModel> rateClothesViewModelList);

    void payAndBackToHomeScreen();

    void showProductState(boolean state);
}
